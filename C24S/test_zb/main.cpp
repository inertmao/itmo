#include <iostream>
#include <vector>

using namespace std;

int main() {
    int n;
    cin >> n;
    int flowers[n];
    for(int i = 0; i < n; ++i) {
        cin >> flowers[i];
    }

    int bestStart = 0, bestLen = 0;
    int currentStart = 0, currentLen = 1;
    int lastFlower = flowers[0], lastFlowerCount = 1;

    for(int i = 1; i < n; ++i) {
        if(flowers[i] == lastFlower) {
            lastFlowerCount++;
            if(lastFlowerCount <= 2) { // Максимум два одинаковых цветка подряд допустимы
                currentLen++;
            } else {
                // Начинаем новый участок с текущего цветка, если встретился третий подряд
                currentLen = 2; // Учитываем текущий и предыдущий одинаковые цветки
                currentStart = i - 1;
            }
        } else {
            currentLen++; // Новый цветок, продолжаем участок
            lastFlower = flowers[i];
            lastFlowerCount = 1; // Сброс счетчика одинаковых цветков
        }
        cout<<bestLen;
        // Обновляем лучший участок, если найден более длинный
        if(currentLen > bestLen) {
            bestLen = currentLen;
            bestStart = currentStart;
        }
    }

    // Выводим результат
    cout << bestStart + 1 << " " << (bestStart + bestLen - 1) + 1 << endl; // Переводим индексы в человеческий формат (начиная с 1)

    return 0;
}
