cmake_minimum_required(VERSION 3.27)
project(test_zb)

set(CMAKE_CXX_STANDARD 17)

add_executable(test_zb main.cpp)
