import os
import numpy as np
import matplotlib.pyplot as plt
from scipy.interpolate import interp1d

os.system("java -jar Lab1.jar")

s = 0
with open("asd.txt") as file:
    array = [row.strip() for row in file]



desired_array = [int(numeric_string) for numeric_string in array]
desired_array.sort()

emp_arr = list(set(desired_array))
emp_arr.sort()
print(emp_arr)
s = len(emp_arr)
m = 0
q = []
t = len(q)
while m < 1 and t != s:
    m = m + (1/s)
    m_round = round(m, 2)
    q.append(m_round)

f = list(set(q))
f.sort()
f.pop()
print(f)

# Ваши данные
x = np.array(emp_arr)
y = np.array(f)

plt.step(x, y, where='post')
# Построение графика
plt.plot(x, y, marker='o')  # Используем маркеры для отображения точек

# Настройка делений на осях
plt.xticks(x)  # Установка делений на оси X в соответствии с вашим массивом x
plt.yticks(y)  # Установка делений на оси Y в соответствии с вашим массивом y

# Добавление подписей к осям и заголовка

plt.title('Эмпирическая функция распределения')

# Отображение графика
plt.grid(True)  # Включение сетки для лучшей читаемости
plt.show()