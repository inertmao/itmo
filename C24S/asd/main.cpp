#include <iostream>
#include <vector>

using namespace std;

int main() {
    int n;
    cin >> n;
    int flowers[n];
    int start = 0;
    int end = 0;
    int maxStart = 0;
    int maxEnd = 1;
    int count = 1;
    if (n == 0){
        cout<<0;
        return 0;
    }
    if (n == 1){
        cout<< 1 << " " << 1;
        return 0;
    } else{
        for(int i = 0; i < n; ++i) {
            cin >> flowers[i];
        }
        int fl1 = flowers[0];
        for(int i = 1; i < n; ++i) {
            if(flowers[i] == fl1) {
                count++;
                if(count <= 2) {
                    maxEnd++;
                } else {
                    maxEnd = 2;
                    maxStart = i - 1;
                }
            } else {
                maxEnd++;
                fl1 = flowers[i];
                count = 1;
            }
            if(maxEnd > end) {
                end = maxEnd;
                start = maxStart;
            }
        }
        cout << start + 1 << " " << (start + end - 1) + 1 << endl;
    }

    return 0;
}
